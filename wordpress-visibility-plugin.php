<?php
/*
Plugin Name: Private Posts Visible to Internal IPs
Plugin URI:  https://gitlab.ebi.ac.uk/ebiwd/wordpress-visibility-plugin/
Description: Make private posts visible to users with internal IP addresses when they are not logged in
Version:     0.0.4
Author:      EMBL-EBI Web Development
Author URI:  https://gitlab.ebi.ac.uk/ebiwd
*/

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

add_action( 'pre_get_posts', 'ebivp_include_private_posts' );


function ebivp_include_private_posts( $query ) {
    if (! $query->is_admin() && $query->is_main_query()) {
        if ( ! is_user_logged_in() && ebivp_is_internal_ip()) {
            $query->set( 'post_status', array( 'publish', 'private' ) );
        }
    }
}

function ebivp_getip(){
    if(isset($_SERVER["REMOTE_ADDR"])){
        return $_SERVER["REMOTE_ADDR"];
    }elseif(isset($_SERVER["HTTP_X_FORWARDED_FOR"])){
        return $_SERVER["HTTP_X_FORWARDED_FOR"];
    }elseif(isset($_SERVER["HTTP_CLIENT_IP"])){
        return $_SERVER["HTTP_CLIENT_IP"];
    }
}

function ebivp_is_internal_ip() {
    $ip = ebivp_getip();
    ebivp_debug_log("User IP: " . $ip);

    $ip_numeric = ip2long($ip);

    $ip_ranges = [
        [ 'min' => "127.0.0.0", 'max' => '127.0.0.255' ],
        [ 'min' => "10.0.0.0", 'max' => '10.255.255.255' ],
        [ 'min' => "192.168.0.0", 'max' => '192.168.255.255' ],
        [ 'min' => "172.16.0.0", 'max' => '172.31.255.255' ]
    ];

    if(ebivp_ip_in_ranges($ip_ranges, $ip_numeric)) {
        ebivp_debug_log("Internal IP");
        return true;
    }
    ebivp_debug_log("External IP");
    return false;
}

function ebivp_ip_in_ranges($ip_ranges, $ip_numeric) {
    foreach ($ip_ranges as $range) {
        if($ip_numeric >= ip2long($range['min']) && $ip_numeric <= ip2long($range['max'])) {
            return true;
        }
    }
    return false;
}

function ebivp_debug_log($message) {
    if ( WP_DEBUG === true ) {
        if ( is_array($message) || is_object($message) ) {
            error_log( print_r($message, true) );
        } else {
            error_log( $message );
        }
    }
}


?>