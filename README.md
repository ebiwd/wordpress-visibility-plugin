# WordPress Plugin to show private posts to internal IP isers

## What does this do?

If a post or page is marked as private this will allow users on internal IP addresses to view the private posts *without* logging in.

Users who are not on internal IP addresses will need to log into WordPress to see private posts as normal.

## How does it work?

It looks for the client IP address, by looking for the following headers (in this order):
 - REMOTE_ADDR
 - HTTP_X_FORWARDED_FOR
 - HTTP_CLIENT_IP

It then checks to see if the IP is in the following ranges
 - 10.0.0.0/8
 - 172.16.0.0/12
 - 192.168.0.0/16

If it is then the user is allowed to see posts with the status of 'publish' or 'private'

The plugin only runs on the non-admin (e.g. frontend of the blog)

## Install
Clone to the WordPress plugins directory.  For example

```
$ cd /var/www/html/wp-content/plugins/
$ git clone git@gitlab.ebi.ac.uk:ebiwd/wordpress-visibility-plugin.git
```

Log into WordPress as an administrator, go to plugins and activate this plugin.

## Caveats and TODO

The headers above are easily spoofed, however this is the same mechanism we're using elsewhere.

 - [x] Ensure the functions are namespaced
 - [ ] Make the IP ranges configurable
 - [ ] Make this work in menus too, so you can put private pages in them and hide the links where appropriate


